﻿#include <iostream>
class Animal
{
public:
    virtual void Voice() const = 0;
};

class Dog : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Woof\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Meow\n";
    }
};

class Chick : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Pipi\n";
    }
};

int main()
{
    Animal* animals[3];
    animals[0] = new Dog();
    animals[1] = new Cat();
    animals[2] = new Chick();

    for (Animal* a : animals)
        a->Voice();
}